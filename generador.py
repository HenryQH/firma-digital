from Crypto.PublicKey import RSA

llave = RSA.generate(1024)

file = open('llavePrivada.pem', 'w')
file.write(llave.exportKey('PEM'))
file.close

file = open('llavePublica.pem', 'w')
file.write(llave.publickey().exportKey('PEM'))
file.close
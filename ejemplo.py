import Crypto
import binascii

from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP

random = Crypto.Random.new().read

private_key = RSA.generate(1024, random)
public_key = private_key.publickey()

#codificamos

private_key = private_key.exportKey(format = 'DER')
public_key = public_key.exportKey(format = 'DER')

private_key = binascii.hexlify(private_key).decode('utf8')
public_key = binascii.hexlify(public_key).decode('utf8')

#decodificamos

private_key = RSA.importKey(binascii.unhexlify(private_key))
public_key = RSA.importKey(binascii.unhexlify(public_key))

mensaje = 'hola'
mensaje = mensaje.encode()

cipher = PKCS1_OAEP.new(public_key)
mensajeEncriptado = cipher.encrypt(mensaje)

print(mensajeEncriptado)

cipher = PKCS1_OAEP.new(private_key)
mensaje = cipher.decrypt(mensajeEncriptado)

print(mensaje)

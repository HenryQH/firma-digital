from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA

mensaje = 'hola'

key = RSA.importKey(open('llavePublica.pem', 'r').read())
crifrado = PKCS1_OAEP.new(key)
cifrar = crifrado.encrypt(mensaje)

f = open('textoEncriptado.txt', 'w')
f.write(cifrar)
f.close
from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA

file = open('textoEncriptado.txt', 'r')
mensaje = file.read()

key = RSA.importKey(open('llavePrivada.pem', 'r').read())
crifrado = PKCS1_OAEP.new(key)
descifrar = crifrado.decrypt(mensaje)

print(descifrar)
